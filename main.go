package main

import (
	log "github.com/sirupsen/logrus"
	"gitlab.com/anguswilliams/clonician/cmd"
)

func main() {
	err := cmd.Execute()
	if err != nil {
		log.Error(err)
	}
}
