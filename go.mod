module gitlab.com/anguswilliams/clonician

go 1.14

require (
	github.com/go-git/go-git/v5 v5.2.0
	github.com/mitchellh/go-homedir v1.1.0
	github.com/sirupsen/logrus v1.2.0
	github.com/spf13/cobra v1.1.3
	github.com/spf13/viper v1.7.0
	github.com/xanzy/go-gitlab v0.47.0
)
