package cmd

import (
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/spf13/viper"
)

var (
	// Used for flags.
	cfgFile string

	rootCmd = &cobra.Command{
		Use:   "clonician",
		Short: "A tool to clone Gitlab group structure",
		Long:  `A tool to clone Gitlab group structure`,
	}

	config Config
)

const GitlabTokenEnv = "GITLAB_TOKEN"

type Config struct {
	Remotes     map[string]RemotesConfig
	Groups      []GroupsConfig
	Workdir     string `mapstructure:"workdir"`
	GitlabToken string
	apiVersion  string
}

type GroupsConfig struct {
	Remote  string
	Workdir string
	Group   string
}

type RemotesConfig struct {
	Url   string
	Token string
	Name  string
}

// Execute executes the root command.
func Execute() error {
	return rootCmd.Execute()
}

func init() {
	cobra.OnInitialize(initConfig)
	rootCmd.PersistentFlags().StringP("gitlabUrl", "u", "https://gitlab.com", "Gitlab Base URL")
	rootCmd.PersistentFlags().StringP("workdir", "d", "~/workspace", "Workdir to clone repos into")

	rootCmd.PersistentFlags().StringVarP(&cfgFile, "config", "c", "", "config file (default is $HOME/.clonician.yaml)")
}

func initConfig() {
	v := viper.NewWithOptions(viper.KeyDelimiter("::"))
	if cfgFile != "" {
		// Use config file from the flag.
		v.SetConfigFile(cfgFile)
	} else {
		// Find home directory.
		home, err := homedir.Dir()
		cobra.CheckErr(err)

		// Search config in home directory with name ".cobra" (without extension).
		v.AddConfigPath(home)
		v.SetConfigName(".clonician")
	}

	v.AutomaticEnv()

	err := v.ReadInConfig()
	if err != nil {
		log.Fatalf("error reading config: %s", err)
		log.Exit(1)
	} else {
		log.Info("using config file: ", v.ConfigFileUsed())
	}

	err = v.Unmarshal(&config)
	if err != nil {
		log.Fatalf("unable to decode into struct, %v", err)
	}
}
