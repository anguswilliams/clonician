package cmd

import (
	"errors"
	"fmt"
	"github.com/go-git/go-git/v5"
	gitConfig "github.com/go-git/go-git/v5/config"
	"github.com/go-git/go-git/v5/plumbing/transport"
	"github.com/mitchellh/go-homedir"
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
	"github.com/xanzy/go-gitlab"
	"os"
	"path/filepath"
)

var cloneCmd = &cobra.Command{
	Use:   "clone",
	Short: "Clone some shit",
	Long:  `Yeah`,
	Run: func(cmd *cobra.Command, args []string) {
		cloneFromArgs(args)
		cloneFromConfig()
	},
}

func init() {
	rootCmd.AddCommand(cloneCmd)
}

func cloneFromArgs(groups []string) {
	workdir, err := homedir.Expand(rootCmd.PersistentFlags().Lookup("workdir").Value.String())
	if err != nil {
		log.Fatalf("Unable to expand directory: %v", err)
	}

	gitlabUrl := rootCmd.PersistentFlags().Lookup("gitlabUrl").Value.String()

	token := os.Getenv(GitlabTokenEnv)

	for _, gid := range groups {
		if err := cloneGitlabGroup(gid, workdir, token, gitlabUrl); err != nil {
			log.Error(err)
		}
	}
}

func cloneFromConfig() {
	for _, gc := range config.Groups {
		workdir := gc.Workdir
		if workdir == "" {
			workdir = config.Workdir
		}

		workdirExpanded, err := homedir.Expand(workdir)
		if err != nil {
			log.Errorf("Unable to expand workdir for group %s: %v", gc.Group, err)
			continue
		}

		remoteConfig, ok := config.Remotes[gc.Remote]
		if !ok {
			log.Errorf("Unable to find remote '%s' config for group %s", gc.Remote, gc.Group)
			continue
		}

		token := remoteConfig.Token
		if remoteConfig.Token == "" {
			token = os.Getenv(GitlabTokenEnv)
		}

		err = cloneGitlabGroup(gc.Group, workdirExpanded, token, remoteConfig.Url)
		if err != nil {
			log.Errorf("Failed to process gitlab group %s: %v", gc.Group, err)
		}
	}
}

func isGitRepo(path string) bool {
	stat, err := os.Stat(filepath.Join(path, ".git"))

	if errors.Is(err, os.ErrNotExist) {
		return false
	}

	if stat.IsDir() {
		return true
	}

	if err != nil {
		log.Error(err)
	}

	return false
}

func cloneGitlabGroup(gid string, workdir string, token string, gitlabUrl string) error {
	gitlabAPI, err := gitlab.NewClient(token, gitlab.WithBaseURL(gitlabUrl))

	if err != nil {
		log.Fatalf("Failed to connect to Gitlab: %v", err)
	}

	includeSubgroups := true
	group, resp, err := gitlabAPI.Groups.ListGroupProjects(
		gid, &gitlab.ListGroupProjectsOptions{IncludeSubgroups: &includeSubgroups})

	if resp != nil && resp.StatusCode == 404 {
		return fmt.Errorf("no such Gitlab group %v", gid)
	}

	if err != nil {
		log.Errorf("something went wrong: %v", err)
	}

	for _, p := range group {
		projDir := filepath.Join(workdir, p.Namespace.FullPath)
		projPath := filepath.Join(projDir, p.Name)
		if isGitRepo(projPath) {
			log.Infof("%v is already a git repo, skipping", projPath)
			continue
		}

		log.Infof("Cloning repo %v to %v", p.SSHURLToRepo, projDir)
		_, err := git.PlainClone(projPath, false, &git.CloneOptions{
			URL:      p.SSHURLToRepo,
			Progress: os.Stdout,
		})

		switch err {
		case git.ErrRepositoryAlreadyExists:
			log.Debugf("repo %v already exists", p.Name)
			continue
		case transport.ErrEmptyRemoteRepository:
			log.Warnf("repo %v is empty, creating new repo and setting remote %v to %v",
				p.Name, git.DefaultRemoteName, p.SSHURLToRepo)
			r, err := git.PlainInit(projPath, false)

			if err != nil {
				_ = os.RemoveAll(projPath)
				log.Errorf("got error trying to create empty repo in %v: %v", projPath, err)
			}

			_, err = r.CreateRemote(&gitConfig.RemoteConfig{Name: git.DefaultRemoteName, URLs: []string{p.SSHURLToRepo}})
			if err != nil {
				log.Errorf(
					"Something went wrong trying to set the remote to %v for %v: %v",
					p.SSHURLToRepo, projPath, err)
			}
			break
		case nil:
			break
		default:
			log.Errorf("got an error trying to clone repo %v: %v", p.SSHURLToRepo, err)
		}
	}

	return nil
}
