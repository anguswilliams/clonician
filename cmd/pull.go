package cmd

import (
	log "github.com/sirupsen/logrus"
	"github.com/spf13/cobra"
)

var pullCmd = &cobra.Command{
	Use:   "pull",
	Short: "Pull repo updates",
	Run: func(cmd *cobra.Command, args []string) {
		log.Fatal("Pull not yet implemented")
	},
}

func init() {
	rootCmd.AddCommand(pullCmd)
}
